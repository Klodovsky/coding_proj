from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView

# only authenticated users will be able to interact with our app
class IndexTemplateView(LoginRequiredMixin, TemplateView):

    def get_template_names(self):
               
        template_name = "index.html"
        return template_name