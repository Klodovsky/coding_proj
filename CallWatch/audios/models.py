from django.db import models
from django.conf import settings


# audio segments model
class Audio_seg(models.Model):
    name = models.CharField(max_length=255, default="")
    slug = models.SlugField(max_length=255, unique=True, default="")
    source = models.CharField(max_length=255, default="")
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    active = models.BooleanField(default=True)     
    #Annotator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,related_name="audios")
    # duration = models.DurationField(max_length=20)                         
    
    def __str__(self):
        return self.name

#Transcriptions model
class Transcriptions(models.Model):
    Transcribed_txt = models.TextField(default="")
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    # updated_at = models.DateTimeField(auto_now=True,name=None)
    
    Audio_seg = models.ForeignKey(Audio_seg, on_delete=models.CASCADE, related_name="transcriptions",default=1)
    Annotator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,default=100)
    
    def __str__(self):
        return self.Transcribed_txt


 class caracset(models.Model):
         Upper = models.TextField(max_length=255)
         Lower = models.TextField(max_length=255)

 