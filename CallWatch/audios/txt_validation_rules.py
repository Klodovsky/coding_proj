import re
from .models import caracset

# gathering diffrent forms of each character set
cset = caracset.objects.all()
Maj = list(cset.Upper)


# Python3 function to Split string into characters 
def split(cset): 
    return [char for char in cset]  


#·	capital letters are allowed only as a first word letter or if all the letters in the word are upper case
def AllUpper(word):
        if ord(word[-1]) not in range(65,90): # checking if Upper through its ascii code
            return False
        if not (set(split(word[0:len(word) - 1])).issubset(set(Maj))):
            return False #Return True if all items set 'word' are present in set 'Maj'
        else:
            return True

#·there can be only zero or one space between two characters

def onlyonezerospace(cset):
    
    x = re.search(".*[[  ],[00]].* | .*[[ ],[00]].* | .*[[  ],[0]].*", cset)
    if not(x) :
     return True

    
#·	characters ?.! should be end of text or followed by one space and an uppercase character

def exlsymbolz(cset):
    x1 = re.search(".*[?.!]$", cset) # ?.! at the end of the text
    x2 = re.search(".*[?.!]\s[A-Z]*$",cset) # ?.! followed by space and Upper
    if ((x1) or (x2)) : return True
    
    else : 
        return False


#·	characters ,;: should be end of text or followed by one space

def dotsymbolz(cset):
    x1 = re.search(".*[,;:]$", cset) # ,;: at the end of the text
    x2 = re.search(".*[,;:]\s[A-Z]*",cset) # ,;: followed by space and Upper
    if ((x1) or (x2)) : return True    
    else : 
        return False
    
    
def ok(cset):
    if (AllUpper(cset) and (dotsymbolz(cset) or exlsymbolz(cset)) and onlyonezerospace(cset)):        
        return True
