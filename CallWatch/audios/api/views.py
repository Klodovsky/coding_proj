from rest_framework import generics, status,viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from audios.models import Audio_seg, Transcriptions
from audios.api.serializers import Audio_segSerializer , TranscriptionsSerializer
from audios.api.permisions import IsToTranscribe
#from audios.api.txt_validation_rules import verif
#from .txt_validation_rules import verif
# from audios.txt_validation_rules import verif

# our audios list view
class AudiosViewSet(generics.CreateAPIView):
    def get(self,request):
       # Query to be updated after creating few tables 
        queryset = Audio_seg.objects.all().order_by("-created_at")
        
        serializer = Audio_segSerializer(queryset, many=True)
        permission_classes = [IsToTranscribe, IsAuthenticated]
        return Response(serializer.data)

# https://www.django-rest-framework.org/api-guide/generic-views/
class AudioDetail(generics.RetrieveUpdateDestroyAPIView):
        queryset = Audio_seg.objects.all()
        serializer_class = Audio_segSerializer


class Transcript(APIView):

        def get(self, request):
         queryset = Transcriptions.objects.all()
         serializer = TranscriptionsSerializer(queryset, many=True)
         return Response(serializer.data)

            # transciption validation form

        #  def submit_txt(self, request):
        #     serializer = TranscriptionsSerializer(data=request.data)
        #     if serializer.is_valid():
        #      if ok((serializer.validated_data['Transcribed_txt'])):
        #         serializer.save()
        #         queryset = Audio_seg.objects.raw()
        #        # "SELECT audio_segm.* FROM Audios as ad 
        #         WHERE NOT EXISTS
        #         (SELECT 1 FROM ad_transcripts WHERE ad_transcripts.audio_id=audio.id) AND 
        #               audio.active="True"")
        #         serializer = Audio_segSerializer(queryset, many=True)
        #         return Response(serializer.data, status=status.HTTP_201_CREATED)
        #     else:
        #         return Response({"Transctiotion error"},
        #          status=status.HTTP_400_BAD_REQUEST)
        #         return Response(serializer.errors,                            
        #           status=status.HTTP_404_BAD_REQUEST)