# from django.urls import include, path
# from rest_framework.routers import DefaultRouter
 #from audios.api import views as av

from django.urls import path, include
from .views import AudiosViewSet
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('audios/',AudiosViewSet.as_view()),
    ]

