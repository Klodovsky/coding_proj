from rest_framework import permissions

class IsToTranscribe(permissions.BasePermission):
    # checks if the audio is transcribed or not 
    def has_object_permission(self,request,view,obj):
        if request.method in permissions.SAFE_METHODS:
            return True
            return obj.active == request.user