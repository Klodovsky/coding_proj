from rest_framework import serializers
from audios.models import Audio_seg, Transcriptions

class Audio_segSerializer(serializers.ModelSerializer):
   
   Annotator = serializers.StringRelatedField(read_only=True)
   created_at = serializers.StringRelatedField(read_only=True)
        
class Meta:
    model = Audio_seg
    exclude = ['source', 'updated_at']
    
    def get_created_at(self, instance):
            return instance.created_at.strftime("%B %d, %Y")



class TranscriptionsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Transcriptions
        exclude = ['Audio_seg', 'Annotator']